import os, shutil, csv
import pprint
import random

#masterpath
from zipfile import ZipFile

masterPath = "/home/valis/Desktop/python_projects/tema1/"

#define variables
no_folders = 2
no_subfolders = 3
no_columns = 4
no_rows = 5
lst = []
temp_name = []

#define functions
def column_names(no_columns):
    for i in range(0, no_columns):
        temp1 = "c" + str(i)
        temp_name.append(temp1)
    return temp_name

def create_dictionary(no_columns,no_rows,temp__column_name):
    for i in range(0,no_rows):
        column_dict = {}
        for j in range(0,no_columns):
            column_dict[temp__column_name[j]] = random.randint(0,10)
        lst.append(column_dict)
    return lst

def create_folders_and_archived_csvs(no_folders,no_subfolders, temp_column_name):
    for i in range(0,no_folders):
        tempPath1 = os.path.join(masterPath, str(i))
        os.mkdir(tempPath1)

        for j in range(0,no_subfolders):
            tempPath2 = os.path.join(tempPath1, str(j))
            os.mkdir(tempPath2)
            os.chdir(tempPath2)

            csvFileName = '%s.csv' %str(j)
            temp_lst = create_dictionary(no_columns, no_rows, temp_column_name)

            with open(csvFileName, 'w') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=temp_column_name)
                writer.writeheader()
                for dicti in temp_lst:
                    writer.writerow(dicti)

            zipFileName = '%s.zip' %str(j)
            with ZipFile(zipFileName, 'w') as zipFile:
                zipFile.write(csvFileName)

            os.chmod(tempPath2, 0o777)
            os.chmod(csvFileName, 0o777)
            os.chmod(zipFileName, 0o777)
            os.remove(csvFileName)
        os.chmod(tempPath1, 0o777)


def delete_folders (no_folders):
    for i in range(0,no_folders):
        tempPath1 = os.path.join(masterPath,str(i))
        if os.path.exists(tempPath1) == True:
            shutil.rmtree(tempPath1,ignore_errors=True)



def read_csvs_from_zip(no_folders, no_subfolders, temp_column_name):

    operation = {'+':[], '-':[], '*':[], '/':[]}

    for i in range(0, len(temp_column_name)):
        if i%4 == 0:
            operation['+'].append(temp_column_name[i])
        elif i%4 == 1:
            operation['-'].append(temp_column_name[i])
        elif i % 4 == 2:
            operation['*'].append(temp_column_name[i])
        else:
            operation['/'].append(temp_column_name[i])
    #print operation

    for i in range(0, no_folders):
        tempPath1 = os.path.join(masterPath, str(i))
        for j in range (0, no_subfolders):
            tempPath2 = os.path.join(tempPath1, str(j))
            os.chdir(tempPath2)
            zipFileName = '%s.zip' %str(j)
            csvFileName = '%s.csv' %str(j)
            with ZipFile(zipFileName, 'r') as zipFile:
                zipFile.extract(csvFileName)
            os.remove(zipFileName)
            os.chmod(csvFileName, 0o777)
            input_file = csv.DictReader(open(csvFileName))
            result_dict = {}

            for dictionary in input_file:
                #print dictionary
                for key_column in dictionary:
                    for key_operation in operation:
                        if key_operation == '+' and key_column in operation[key_operation]:
                            if result_dict.has_key(key_column):
                                result_dict[key_column] += int(dictionary[key_column])
                            else:
                                result_dict[key_column] = int(dictionary[key_column])
                        elif key_operation == '-' and key_column in operation[key_operation]:
                            if result_dict.has_key(key_column):
                                result_dict[key_column] -= int(dictionary[key_column])
                            else:
                                result_dict[key_column] = int(dictionary[key_column])
                        elif key_operation == '*' and key_column in operation[key_operation]:
                            if result_dict.has_key(key_column):
                                result_dict[key_column] *= int(dictionary[key_column])
                            else:
                                result_dict[key_column] = int(dictionary[key_column])
                        elif key_operation == '/' and key_column in operation[key_operation]:
                            if int(dictionary[key_column]) != 0:
                                #print "gigi ", dictionary[key_column]
                                #print "gigi1 ", isinstance(dictionary[key_column], str)
                                if result_dict.has_key(key_column):
                                    result_dict[key_column] /= float(dictionary[key_column])
                                else:
                                    result_dict[key_column] = float(dictionary[key_column])
            #print result_dict
    resultsCsv = 'resultsCSV.csv'
    os.chdir(masterPath)
    with open(resultsCsv, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames = temp_column_name)
        writer.writeheader()
        writer.writerow(result_dict)
    os.chmod(masterPath + resultsCsv, 0o777)

#call functions
temp_column_name = column_names(no_columns)
delete_folders(no_folders)
create_folders_and_archived_csvs(no_folders,no_subfolders,temp_column_name)
read_csvs_from_zip(no_folders,no_subfolders,temp_column_name)


#change permision to file so it can be accessed by anyone
os.chmod(masterPath + "tema1.py", 0o777)
os.chmod(masterPath, 0o777)

#create_dictionary(no_columns,no_rows)